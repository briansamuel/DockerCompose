-- example HTTP POST script which demonstrates setting the
-- HTTP method, body, and adding a header
wrk.method = "GET"
-- wrk.body   = '{"value":"ourdata","type":"example"}'
wrk.headers["Authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJfaWQiOjQsInJvbGUiOiJ1c2VyIn0sImV4cCI6MTY2MDUzNDc1MywiaWF0IjoxNjU3OTQyNzUzfQ.enlyg6Ntgdjvz0MXqnxypUN50oruspC7vLy5SZf37U8"